﻿using System;

namespace CoreHWApp
{
		internal class Program
		{
			static void Main(string[] args)
			{
				var card = new CardVisa(VisaCardComissionCalc);
				card.Notify += NotifyHandler;

				card.AddMoney(6);
				card.Withdrawal(4);
				card.Withdrawal(2);
				card.AddMoney(3);
				card.Withdrawal(4);

				static void NotifyHandler(string messege)
				{
					Console.WriteLine(messege);
				}
				static decimal VisaCardComissionCalc(decimal money)
				{
					return money * 0.05m;
				}

				var card2 = new VisaGold(VisaCardComissionCalc2);
				card2.Notify += NotifyHandler2;

				card2.AddMoney(13);
				card2.Withdrawal(9);
				card2.Withdrawal(4);
				card2.AddMoney(3);
				card2.Withdrawal(2);

				static void NotifyHandler2(string messege)
				{
					Console.WriteLine(messege);
				}
				static decimal VisaCardComissionCalc2(decimal money)
				{
					return money * 0.01m;
				}

			}
		}
	}

