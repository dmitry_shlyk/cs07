﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreHWApp
{
	public abstract class Card
	{
		public delegate void AcccountHandler(string message);

		public delegate decimal CommissionHandler(decimal money);

		protected CommissionHandler commissionHandler;

		public event AcccountHandler Notify;

		public decimal Account { get; protected set; }

		protected Card(CommissionHandler commissionHandler)
		{
			this.commissionHandler = commissionHandler;
		}
		public void StartMoney(decimal money)
		{
			Account = money;
		}
		public abstract void AddMoney(decimal money);
		public abstract void Withdrawal(decimal money);
		protected virtual void Notify1(string message)
		{
			Notify.Invoke(message);
		}
	}
}

