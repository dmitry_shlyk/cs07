﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreHWApp
{
	internal class VisaGold : Card
	{
		public VisaGold(CommissionHandler commissionHandler) : base(commissionHandler)
		{

		}
		public override void AddMoney(decimal money)
		{
			decimal totalMoney = money - commissionHandler(money);
			Account += totalMoney;
			Notify1($"На счет поступило:{totalMoney}");
		}
		public override void Withdrawal(decimal money)
		{
			decimal moneyWithComission = money + commissionHandler(money);
			if (Account >= moneyWithComission)
			{
				Account -= moneyWithComission;
				Notify1($"Со счета снято:{moneyWithComission},Текущий баланс:{Account}");
			}
			else
			{
				Notify1($"Недостаточно денег на счете. Текущий баланс:{Account}");
			}
		}
	}

}

